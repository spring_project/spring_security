/*
 * Copyright (C) HAND Enterprise Solutions Company Ltd.
 * All Rights Reserved
 */

package com.security.demo.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author WangSen
 * @Title TimeInterceptor
 * @Description
 * @date: 2019-1-18 11:27
 */
@Component
public class TimeInterceptor implements HandlerInterceptor {

    /**
     * 方法里面可以获取到请求controller的方法名，因此可以针对不同的情况，对controller接口进行请求拦截。
     * @param request
     * @param response
     * @param handler
     * @return true 则进行请求访问，false进行请求拦截。
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("preHandle");
        System.out.println(((HandlerMethod)handler).getBean().getClass().getName());
        System.out.println(((HandlerMethod)handler).getMethod().getName());

        request.setAttribute("startTime", System.currentTimeMillis());
        return true;
    }

    /**
     * 请求执行完成后调用的方法。值得注意的是，当请求方法发生错误的时候，改方法不会被调用。
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        System.out.println("postHandle");
        Long start = (Long) request.getAttribute("startTime");
        System.out.println("time interceptor 耗时:"+ (System.currentTimeMillis() - start));

    }

    /**
     * 请求完成后调用的方法。请求方法出错，同样会调用此方法。
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        System.out.println("afterCompletion");
        Long start = (Long) request.getAttribute("startTime");
        System.out.println("time interceptor 耗时:"+ (System.currentTimeMillis() - start));
        System.out.println("ex is "+ex);

    }
}