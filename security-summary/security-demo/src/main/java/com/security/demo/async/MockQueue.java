/*
 * Copyright (C) HAND Enterprise Solutions Company Ltd.
 * All Rights Reserved
 */

package com.security.demo.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author WangSen
 * @Title MockQueue
 * @Description
 * @date: 2019-1-22 15:55
 */
@Component
public class MockQueue {
    private final static Logger logger = LoggerFactory.getLogger(MockQueue.class);

//    相当于结果集的队列。
    String completeOrder ;

    public void setOrder(String numer){
        new Thread(()->{
            logger.info("开始处理线程");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.completeOrder = numer;
            logger.info("订单处理结束");

        }).start();
    }

    public String getCompleteOrder() {
        return completeOrder;
    }

    public void setCompleteOrder(String completeOrder) {
        this.completeOrder = completeOrder;
    }
}