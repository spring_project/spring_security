/*
 * Copyright (C) HAND Enterprise Solutions Company Ltd.
 * All Rights Reserved
 */

package com.security.demo.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author WangSen
 * @Title MyValidator
 * @Description 改类实现ConstraintValidator接口之后，会自动注入到容器中去。（不需要加额外注解。）
 * @date: 2019-1-11 10:18
 */
public class MyValidator implements ConstraintValidator<MyConstraint,Object> {

    @Override
    public void initialize(MyConstraint myConstraint) {
        System.out.println("init "+myConstraint.toString());
    }
    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        System.out.println("需要验证的数据为 "+o);
        return false;
    }
}