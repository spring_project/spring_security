/*
 * Copyright (C) HAND Enterprise Solutions Company Ltd.
 * All Rights Reserved
 */

package com.security.demo.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author WangSen
 * @Title FileController
 * @Description
 * @date: 2019-1-21 15:54
 */
@RestController
public class FileController {

    private String folder = "D:\\study\\spring-security\\mine-git\\spring_security\\security-summary\\security-demo\\src\\main\\java\\com\\security\\demo\\controller";

    @PostMapping("/upload")
    public String upload(MultipartFile file) throws Exception {

        // 表单名字
        System.out.println(file.getName());
        // 文件名字
        System.out.println(file.getOriginalFilename());
        System.out.println(file.getSize());
        File localFile = new File(folder, "sen.txt");
//        将文件传输到目标文件
        file.transferTo(localFile);
        return localFile.getAbsolutePath();
    }

    @GetMapping("/downLoad")
    public void download(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try (InputStream inputStream = new FileInputStream(new File(folder, "sen.txt"));
             OutputStream outputStream = response.getOutputStream();) {

            response.setContentType("application/x-download");
            response.addHeader("Content-Disposition", "attachment;filename=test.txt");
            IOUtils.copy(inputStream, outputStream);
            outputStream.flush();
        }
    }


}