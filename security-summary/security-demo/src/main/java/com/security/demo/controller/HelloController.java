/*
 * Copyright (C) HAND Enterprise Solutions Company Ltd.
 * All Rights Reserved
 */

package com.security.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author WangSen
 * @Title HelloController
 * @Description
 * @date: 2018-12-16 17:34
 */
@RestController
public class HelloController {

    @GetMapping("/sayHello")
    public String sayHello() {
        return "hello";
    }

}