/*
 * Copyright (C) HAND Enterprise Solutions Company Ltd.
 * All Rights Reserved
 */

package com.security.demo.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.security.demo.dto.User;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * @author WangSen
 * @Title UserController
 * @Description
 * @date: 2018-12-23 15:35
 */
@RestController
public class UserController {
    @GetMapping("/user")
    @JsonView(User.SimpleView.class)
    public List<User> query() throws Exception {
//        throw new Exception();
        List list = new ArrayList();
        User user = new User();
        user.setName("tom");
        user.setAge(20);
        user.setGender("female");
        list.add(user);
        return list;
    }

    @GetMapping("/user/detail")
    @JsonView(User.DetailView.class)
    public User detail() {
        User user = new User();
        user.setName("tom");
        user.setAge(20);
        user.setGender("female");
        return user;
    }

    /**
     * 更新操作
     * @return
     */
    @PutMapping("/user")
    public User update(@Valid @RequestBody User user, BindingResult result) {
        System.out.println(result);
        System.out.println(user);
        return user;
    }
}












