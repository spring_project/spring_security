/*
 * Copyright (C) HAND Enterprise Solutions Company Ltd.
 * All Rights Reserved
 */

package com.security.demo.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.Callable;

/**
 * @author WangSen
 * @Title AsyncController
 * @Description
 * @date: 2019-1-22 14:19
 */
@RestController
public class AsyncController {
    private static final Logger logger = LoggerFactory.getLogger(AsyncController.class);
    @Autowired
    private MockQueue mockQueue;
    @Autowired
    private DeferredResultHolder deferredResultHolder;
    @GetMapping("/async")
    public Callable<String> async() throws Exception {
        logger.info("主线程开始");
        Callable<String> result = new Callable<String>() {
            @Override
            public String call() throws Exception {
                logger.info("副线程开始");
                Thread.sleep(2000);
//                Thread.sleep(4000);
                logger.info("副线程返回");
                return "success";
            }
        };
        return result;
    }

    @GetMapping("/async2")
    public DeferredResult<String> async2() throws Exception {
        logger.info("主线程开始");
        // 第一步，将发送消息到消息队列，并处理逻辑。（这里没有发送消息的消息队列，直接进行逻辑处理，并将处理结果放到队列[completeOrder]里）
        mockQueue.setOrder(String.valueOf(2));

        // 第二部，监听返回结果，获取返回数据。（主要注意DeferredResult类的用法。）
        DeferredResult<String> result = new DeferredResult<>();
        deferredResultHolder.getMap().put(String.valueOf(2), result);

        return result;
    }


}











