/*
 * Copyright (C) HAND Enterprise Solutions Company Ltd.
 * All Rights Reserved
 */

package com.security.demo.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.security.demo.validator.MyConstraint;

import javax.validation.constraints.NotNull;

/**
 * @author WangSen
 * @Title User
 * @Description
 * @date: 2018-12-23 15:13
 */
public class User {
    public interface SimpleView{}
    public interface DetailView extends SimpleView{}
    @NotNull(message = "id不可为空")
    Long id;

    @MyConstraint
    String name;

    Integer age;
    String gender;
    @JsonView(SimpleView.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonView(SimpleView.class)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonView(DetailView.class)
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    @JsonView(SimpleView.class)
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }
}