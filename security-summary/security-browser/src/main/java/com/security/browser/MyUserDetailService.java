/*
 * Copyright (C) HAND Enterprise Solutions Company Ltd.
 * All Rights Reserved
 */

package com.security.browser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @author WangSen
 * @Title MyUserDetailService
 * @Description
 * @date: 2019-1-23 20:18
 */
@Component
public class MyUserDetailService implements UserDetailsService {

    private final static Logger logger = LoggerFactory.getLogger(MyUserDetailService.class);
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        logger.info("登录用户为: {}",s);
        return new User("admin","123", AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
    }
}
